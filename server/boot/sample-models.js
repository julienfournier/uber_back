// Copyright IBM Corp. 2015,2016. All Rights Reserved.
// Node module: loopback-example-access-control
// This file is licensed under the Artistic License 2.0.
// License text available at https://opensource.org/licenses/Artistic-2.0

module.exports = function (app) {
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  var Team = app.models.Team;
  var Car = app.models.Car;
  var User = app.models.User;

  User.create([
    {username: 'Julien', email: 'julien@uber.com', password: 'password'},
  ], function (err, users) {
    if (err) return err;
    //...
    // Create projects, assign project owners and project team members
    //...
    // Create the admin role
    Role.create({
      name: 'admin'
    }, function (err, role) {
      if (err) return err;
      debug(role);

      // Make Bob an admin
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: users[0].id
      }, function (err, principal) {
        if (err) return err;
        debug(principal);
      });
    });
  });

  Car.create({
    brand: 'Peugeot',
    model: '208',
    rating: 4
  }, function (err, car) {
    console.log('Created car:', car);
  });
};
