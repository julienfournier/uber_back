// Copyright IBM Corp. 2015,2016. All Rights Reserved.
// Node module: loopback-example-access-control
// This file is licensed under the Artistic License 2.0.
// License text available at https://opensource.org/licenses/Artistic-2.0

module.exports = function (app) {
  var router = app.loopback.Router();

  router.get('/', function (req, res) {
    res.render('index', {
      loginFailed: false
    });
  });

  router.get('/matrix/:departure/:arrival', function (req, res) {
    console.log('ok');
    console.log(req.params);
    var departure = req.params.departure;
    var arrival = req.params.arrival;
    var date = Date.now();

    var moment = require('moment');
    var request = require('request');

    request({
      method: 'GET',
      url: 'https://api.openrouteservice.org/matrix?api_key=5b3ce3597851110001cf6248cca3a589f852410bb9b3c5399de6d221&profile=driving-car&locations=' + departure + '%7C' + arrival + '&metrics=distance%7Cduration&units=m',
      headers: {
        'Accept': 'application/json; charset=utf-8'
      }
    }, function (error, response, body) {
      if (response.statusCode === 200) {
        var jsonObject = JSON.parse(body);
        var distance = jsonObject.distances[0][1];
        var duration = jsonObject.durations[0][1];
        var price = 1.2 + ((distance / 1000) * 1.2) + ((duration / 60) * 0.40);
        var estimatedArrival = moment(date).add('seconds', duration).format('HH:mm:ss');

        if (price < 6) {
          price = 6;
        }

        res.json({
          status: 'OK',
          distance: distance,
          duration: duration,
          price: price,
          departure: departure,
          arrival: arrival,
          estimatedArrival: estimatedArrival
        });
      } else {
        res.render({status: 'KO'});
      }


    });
  });


  router.get('/projects', function (req, res) {
    res.render('projects');
  });

  router.post('/projects', function (req, res) {
    var email = req.body.email;
    var password = req.body.password;

    app.models.User.login({
      email: email,
      password: password
    }, 'user', function (err, token) {
      if (err)
        return res.render('index', {
          email: email,
          password: password,
          loginFailed: true
        });

      token = token.toJSON();

      res.render('projects', {
        username: token.user.username,
        accessToken: token.id
      });
    });
  });

  router.post('/register', function (req, res) {
    var type = req.body.type;
    var email = req.body.email;
    var password = req.body.password;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var User = app.models.User;

    User.create({email: email, password: password},
      function (err, user) {
        console.log(user);
        console.log('User Id : ' + user.id);

        app.models.Profile.create({
          userId: user.id,
          firstname: firstname,
          lastname: lastname,
          type: type
        }, function (err, profile) {
          console.log(profile);
          console.log(user.id);
        });
      });
  });

  router.get('/logout', function (req, res) {
    var AccessToken = app.models.AccessToken;
    var token = new AccessToken({id: req.query['access_token']});
    token.destroy();

    res.redirect('/');
  });

  app.use(router);
};
